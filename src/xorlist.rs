#![allow(unused)]

use std::ptr;
use std::mem;

/// A doubly-linked list that uses xor-ed pointers instead of a pair of pointer to keep track of
/// the next and previous references.  It also cleans up dangling references when dropped, of
/// course, but it still takes O(n) time as with any linked list.
///
/// Has O(1) time for adding to the end of the list as it keeps a reference to the last element.
pub struct XorList<T> {
    first: *mut XorEntry<T>,
    last: *mut XorEntry<T>
}

struct XorEntry<T> {
    ptr_info: usize,
    data: T
}

impl<T> XorList<T> {
    /// Creates a new empty XorList.
    pub fn new() -> XorList<T> {
        XorList {
            first: ptr::null_mut(),
            last: ptr::null_mut()
        }
    }

    /// Pushes a new element onto the end of the list, updating pointers.
    pub fn push(&mut self, item: T) {
        if self.first.is_null() {

            // Make the container for the list element.
            let raw = Box::into_raw(Box::new(XorEntry {
                ptr_info: 0, // 0 ^ 0 == 0
                data: item
            }));

            unsafe {
                // Then put it in both the first and last elements.
                let p = raw as *mut XorEntry<T>;
                self.first = p;
                self.last = p;
            }
        } else {
            unsafe {
                let last_prev = (*self.last).ptr_info as *mut XorEntry<T>;
                let new_raw = Box::into_raw(Box::new(XorEntry {
                    ptr_info: self.last as usize,
                    data: item
                }));

                // Update the old last to point to the new one.
                (*self.last).ptr_info = entry_ptr_info_between(last_prev, new_raw);

                // Actually update the last pointer.
                self.last = new_raw;
            }
        }
    }

    /// Pops the last element off the list, if there's one there, and updates pointers.
    pub fn pop(&mut self) -> Option<T> {
        if self.last.is_null() {
            return None
        }

        unsafe {
            if self.last == self.first {

                // This is easy, just erase everything and pull out the reference.
                let i = Box::from_raw(self.last);
                self.first = ptr::null_mut();
                self.last = ptr::null_mut();
                Some(i.data)

            } else {
                // Do some juggling to update references.
                let last_prev = entry_next(ptr::null_mut(), self.last);
                let last_prev_prev = entry_next(self.last, last_prev);
                (*last_prev).ptr_info = last_prev_prev as usize;

                // Reconstruct the original box for the entry, then update with new last pointer.
                let dangling = Box::from_raw(self.last);
                self.last = last_prev;

                // And return the popped item.
                Some(dangling.data)
            }
        }
    }

    /// Returns the length of the list.  O(n) time.
    pub fn len(&self) -> usize {
        if self.first.is_null() {
            return 0
        }
        let mut l = 1;
        let mut prev = ptr::null_mut();
        let mut at = self.first;
        while at != self.last {
            l += 1;
            let next = unsafe { entry_next(prev, at) };
            prev = at;
            at = next;
        }
        return l;
    }

    /// Returns a reference to the last element of the list, if there's one there.  O(1) time.
    pub fn last(&self) -> Option<&T> {
        if !self.last.is_null() {
            unsafe { Some(&(*self.last).data) }
        } else {
            None
        }
    }

    /// Returns a mutable refernce to the last element of the list, if there's one there.  O(1) time.
    pub fn last_mut(&mut self) -> Option<&mut T> {
        if !self.last.is_null() {
            unsafe { Some(&mut (*self.last).data) }
        } else {
            None
        }
    }
}

#[inline(always)]
unsafe fn entry_next<T>(prev: *mut XorEntry<T>, cur: *mut XorEntry<T>) -> *mut XorEntry<T> {
    ((*cur).ptr_info ^ prev as usize) as *mut XorEntry<T>
}

#[inline(always)]
fn entry_ptr_info_between<T>(before: *mut XorEntry<T>, after: *mut XorEntry<T>) -> usize {
    before as usize ^ after as usize
}

impl<T> Drop for XorList<T> {
    fn drop(&mut self) {
        let mut prev = ptr::null_mut();
        let mut at = self.first;
        while !at.is_null() {
            unsafe {
                // Just reconstruct the box and drop it.
                mem::drop(Box::from_raw(at));

                // Move on to the next element.
                let next = entry_next(prev, at);
                prev = at;
                at = next;
            }
        }
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_push() {

        let mut l = XorList::new();
        l.push(0);
        assert_eq!(0, *l.last().unwrap())

    }

    #[test]
    fn test_pop() {

        let mut l = XorList::new();
        l.push(0u32);
        l.push(1u32);
        l.push(42u32);

        assert_eq!(3, l.len());
        assert_eq!(42, *l.last().unwrap());
        assert_eq!(Some(42), l.pop());
        assert_eq!(Some(1), l.pop());
        assert_eq!(Some(0), l.pop());
        assert_eq!(None, l.pop());

    }

}
